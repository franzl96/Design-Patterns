
/*
Beobachter Interface zur Identifizierung beim zu beobachtbaren Objekt und einer Reaktion auf die änderung des beobachtbaren Objektes
 */
public interface Observer {
    void update(Observable o, Object info);
}
