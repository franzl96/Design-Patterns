/*
Das hier ist der Beobachter
 */

public class ConcreteObserver implements Observer {


    @Override
    public void update(Observable o, Object info) {
        System.out.println(info.toString());
    }
}
