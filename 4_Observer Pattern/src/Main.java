public class Main {

    public static void  main(String... args){

        //Anlegen eines zu beobachtenden Objektes
        ConcreteObservable pinguin = new ConcreteObservable();

        //Anlegen von Beobachtern
        ConcreteObserver forscherHans = new ConcreteObserver();
        ConcreteObserver forscherFlorian = new ConcreteObserver();
        ConcreteObserver forscherAnna = new ConcreteObserver();

        //Registrieren von Beobachtern
        pinguin.addObserver(forscherHans);
        pinguin.addObserver(forscherFlorian);
        pinguin.addObserver(forscherAnna);

        //Informieren der Beobachter
        pinguin.notifyObservers("Ich Schlafe jetzt!");

    }
}
