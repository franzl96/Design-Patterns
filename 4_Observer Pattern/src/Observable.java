import java.util.ArrayList;

/*
Das beobachtbare Objekt mit seinen beobachtern, welche das Interface Beobachter Implementieren
 */

abstract class Observable {

    //Liste aller Observer
    ArrayList<Observer> observers = new ArrayList<>();


    void addObserver(Observer o){
        observers.add(o);
    }

    void deleteObserver(Observer o){
        observers.remove(o);
    }

    //Weitergabe der Information an den Observer
    void notifyObservers(Object info){
        for(Observer o: observers){
            o.update(this,info);
        }
    }






}
