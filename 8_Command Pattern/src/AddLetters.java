public class AddLetters implements Command {

    private StringBuffer text;
    private String string;

    public AddLetters(StringBuffer text, String string){
        this.text = text;
        this.string = string;
    }

    @Override
    public void execute() {
        this.text.append(string);
    }

    @Override
    public void undo() {
        this.text.deleteCharAt(text.length() - 1);
    }
}
