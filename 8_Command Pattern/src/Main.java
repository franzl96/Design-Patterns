import java.util.Stack;

public class Main {

    static StringBuffer stringBuffer = new StringBuffer();

    static Stack<Command> history = new Stack<>();

    public static void main(String[] args) {


        for(int i = 0; i < 10; i++) {
            Command addLetters = new AddLetters(stringBuffer, i % 2 == 0 ? "A" : "B");
            addLetters.execute();
            history.push(addLetters);
        }

        for(int j = 0; j < 3; j++) {
            history.pop().undo();
        }

        System.out.println(stringBuffer.toString());

    }
}
