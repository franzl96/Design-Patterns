public interface Command {

    public void execute();

    default void undo(){
        throw new UnsupportedOperationException();
    }

}
