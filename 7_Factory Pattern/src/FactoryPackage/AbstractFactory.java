package FactoryPackage;

public interface AbstractFactory {

    public AbstractProductA createProductA();
    public AbstractProductB createProductB();

}
