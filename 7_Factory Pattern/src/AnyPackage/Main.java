package AnyPackage;

import FactoryPackage.*;

public class Main {

    public static void main(String[] args) {

       AbstractFactory factory1 = new ConcreteFactory1();
       AbstractFactory factory2 = new ConcreteFactory2();

       AbstractProductA productA1 = factory1.createProductA();
       AbstractProductA productA2 = factory2.createProductA();
       AbstractProductB productB1 = factory1.createProductB();
       AbstractProductB productB2 = factory2.createProductB();

       printProductsA(productA1);
       printProductsA(productA2);
       printProductsB(productB1);
       printProductsB(productB2);






    }

    public static void printProductsA(AbstractProductA productA){
        System.out.println(productA.getAString());
    }

    public static void printProductsB(AbstractProductB productB){
        System.out.println(productB.getBString());
    }

}
