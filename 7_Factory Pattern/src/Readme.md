##Factory-Pattern

Das Factory Patten macht es möglich die Instanziierung zwischen ConcreteFactory und
Products zu verbergen.
Es ist nützlich, wenn man Objekte erzeugen muss, die einen besonders langen Konstruktor haben.
Damit kann man sich viel Schreibarbeit sparen, wenn man mehrere Objekte mit nur in wenigen oder keinen
Punkten abweichenden Eigenschaften zu erstellen.

**Beispiel:**

* API Wrappers
* Iterators
* Formen und dessen Zeichnung