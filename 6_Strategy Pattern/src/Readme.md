##Strategy-Pattern

Das Strategy Pattern hilft dabei, um über eine Schnittstelle einen Aufruf einer
speziellen Funktion über einen gemeimsamen Funktiosaufruf zu machen.\
Hierfür wird dann einfach eine spezielle Klasse die das richtige Interface
implementiert, und somit vom richtigen Typ ist, übergeben.

**Beispiel:**
* Comperator -> Übergabe der Annonymen Klasse an die Sort-Funktion von List Objekten
* (Java Kara...)