#Singleton-Pattern
Die Idee hinter dem Singelton ist, dass nur eine Instanz der kompletten Klasse
existiert.\
**Beispiele:**
* System.out -> bei verfügbarkeit nur einer Konsole
* Handlers
* Datenbankenmanager
