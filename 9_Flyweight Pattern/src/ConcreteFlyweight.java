/**
 * Das ist die Konkrete Implementierung der Flyweight Klasse. Das kann nun ein gewisses Objekt
 * wie Grafiken mit Bilder, beispielsweise ein Like Button sein.
 */
public class ConcreteFlyweight implements Flyweight{

    public ConcreteFlyweight(String intrinsicThing, String extrinsicThing){
        this.intrinsicThing = intrinsicThing;
    }


    String intrinsicThing;

    int extrinsicThing;
}
