import java.util.*;

public class FlyweightFactory {

    int count = 0;

    private Map<String, String> cache = new HashMap<>();

    String leichtesObjekt = "ExternalFactor"; //Das leichte Objekt als eine Art externe eigenschaft, die sich ändert.




    public Flyweight createObjects(String schweresObjektDasGeladenWerdenSoll){

        //Prüfe ob das Objekt bereits im Cache liegt!
        if(cache.containsKey(schweresObjektDasGeladenWerdenSoll)){
            return new ConcreteFlyweight(leichtesObjekt, cache.get(schweresObjektDasGeladenWerdenSoll)); //Ausgabe und keine erneute erzeugung des Objekts
        }
        //Wenn noch nicht in Cache, dann...
        String dasSchwereObjekt = "new Der Konkrete aufruf des Objektes!!"; //Erzeuge das Objekt

        cache.put(schweresObjektDasGeladenWerdenSoll, dasSchwereObjekt); //Lege es in den Cache

        return new ConcreteFlyweight(leichtesObjekt, dasSchwereObjekt); //Gib nun das erzeugte Objekt zurück
    }







}
